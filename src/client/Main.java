package client ;



import javax.naming.InitialContext;
import javax.naming.NamingException ;

import ejb.sessions.DomaineInconnuException;
import ejb.sessions.EcoleDejaAttacheeException;
import ejb.sessions.EcoleDejaCreeeException;
import ejb.sessions.EcoleInconnueException;
import ejb.sessions.ServiceEcolesPolytechRemote;
import ejb.sessions.SpecialiteExisteDejaException;
import ejb.entites.EcolePolytech ;
import ejb.entites.Specialite ;
import ejb.entites.Domaine ;

public class Main {
	
	
 public static void main(String[] args)  { 
   try {
      InitialContext ctx = new InitialContext();
      System.out.println("Accès au service distant") ;
      String adresse="ejb:sitesPolytech/sitesPolytechSessions/ServiceEcolesPolytechBean!ejb.sessions.ServiceEcolesPolytechRemote";
      
      Object obj = ctx.lookup(adresse);
      
      ServiceEcolesPolytechRemote service = (ServiceEcolesPolytechRemote) obj ;
      try {
        service.creerEcolePolytech("Lille","http://www.polytech-lille.fr", 50.60759,3.13604) ;
        for (Domaine d : service.getDomaines()) {
       	 if (d.getId()!=3) 
       	   service.attacherDomaine(d.getId(), "Lille"); 
        }
        service.addSpecialite("Lille", "Informatique et Statistique", "IS", 
	              ServiceEcolesPolytechRemote.TypeSpec.INITIALE);
        service.addSpecialite("Lille", "Informatique et Statistique Appr", "IS2A", 
        		ServiceEcolesPolytechRemote.TypeSpec.APPRENTISSAGE);
      } catch(EcoleDejaCreeeException e) {
    	  System.err.println("Erreur: Ecole Existante : Lille") ;
      } catch (DomaineInconnuException e) {
    	  System.err.println("Erreur: Domaine inconnu") ;
	  } catch (EcoleDejaAttacheeException e) {
		System.err.println("Erreur: école déjà attachée") ;
	  } catch (SpecialiteExisteDejaException e) {
		System.err.println("Erreur: spécialité existe déjà") ;
	  } catch (EcoleInconnueException e) {
	  	System.err.println("Erreur: école inconnue") ;
		}
      
      for (Domaine d : service.getDomaines()) {
    	  System.out.println("Ecoles du domaine "+d.getNom()+" :") ;
    	  for (EcolePolytech ecole : d.getEcoles()) System.out.println(" - "+ecole.getNom()) ;
      }
      System.out.println("Les écoles au Nord de Paris-UPMC :") ;
      for (EcolePolytech ecole : service.getEcolesPolytechAuNordDe("Paris-UPMC"))
        System.out.println("ecole : "+ecole.getNom()) ;
      System.out.println("Les écoles et leurs spécialités :") ;
      for (EcolePolytech ecole : service.getEcolesPolytech()){
        System.out.println("ecole : "+ecole.getNom()) ;
        for (Specialite spec : ecole.getSpecialites()){
        	System.out.println(" - specialite :"+spec.getNom()+" ("+spec.getAcronyme()+")");
        }
      }
    } catch(NamingException e3) {
        System.err.println("error:"+e3.getMessage() ) ;
	} catch (EcoleInconnueException e) {
		System.err.println("Erreur: école inconnue") ;
		}  
  }	
}

