package ejb.sessions;


import ejb.entites.Domaine;
import ejb.entites.EcolePolytech ;
import ejb.entites.Specialite;
import ejb.entites.SpecialiteApprentissage;
import ejb.entites.SpecialiteFC;
import ejb.entites.SpecialiteInitiale;

import jakarta.persistence.* ;

import java.util.Collection ;

@jakarta.ejb.Stateless()
public class ServiceEcolesPolytechBean 
  implements ServiceEcolesPolytechLocal, ServiceEcolesPolytechRemote  {

  @PersistenceContext(unitName="sitesPolytech") protected EntityManager em ;

  public ServiceEcolesPolytechBean() { }
    
  public void creerEcolePolytech(String nom,String siteWeb, double latitude,
			double longitude) throws EcoleDejaCreeeException {
	try {
		this.getEcole(nom) ;
		throw new EcoleDejaCreeeException() ;
	} catch(EcoleInconnueException e) {
		EcolePolytech ecole=new EcolePolytech() ;
		ecole.setNom(nom) ;
		ecole.setSiteWeb(siteWeb) ;
		ecole.setLatitude(latitude) ;
		ecole.setLongitude(longitude) ;
		em.persist(ecole) ;
	}	
  }

  

  public EcolePolytech getEcole(String nom) throws EcoleInconnueException {
	EcolePolytech ecole=null ;
	ecole = (EcolePolytech) em.find(EcolePolytech.class, nom) ;
	if (ecole==null) throw new  EcoleInconnueException() ;
	return ecole;
  }


  @SuppressWarnings("unchecked")
  public Collection<EcolePolytech> getEcolesPolytech() {
	Collection<EcolePolytech> resultList = (Collection<EcolePolytech>) em.createQuery("from EcolePolytech")
			.getResultList();
	return resultList ;
  }


  @SuppressWarnings("unchecked")
  public Collection<Domaine> getDomaines() {
	Collection<Domaine> resultList = (Collection<Domaine>) em.createQuery("from Domaine")
			.getResultList();
	return resultList ;
  }


  public void attacherDomaine(int domaineId, String nomEcole)
		throws DomaineInconnuException, EcoleInconnueException,
		EcoleDejaAttacheeException {
	Domaine dom = this.getDomaine(domaineId) ;
	EcolePolytech ecole = this.getEcole(nomEcole) ;
	if (dom.getEcoles().contains(ecole))
		throw new EcoleDejaAttacheeException() ;
	dom.getEcoles().add(ecole) ;
  }

  private Domaine getDomaine(int domaineId) throws DomaineInconnuException {
	Domaine domaine=null ;
	domaine = (Domaine) em.find(Domaine.class, domaineId) ;
	if (domaine==null) throw new  DomaineInconnuException() ;
	return domaine;
  }

@SuppressWarnings("unchecked")
@Override
public Collection<EcolePolytech> getEcolesPolytechAuNordDe(String nom) throws EcoleInconnueException {
	EcolePolytech ecole=this.getEcole(nom) ;
	return (Collection<EcolePolytech>) em.createQuery(
	    		"select e from EcolePolytech e where e.latitude>:latit")
			.setParameter("latit",ecole.getLatitude()).getResultList() ;
}

@Override
public void addSpecialite(String nomEcole, String nomSpecialite, String acronyme, TypeSpec type)
		throws EcoleInconnueException, SpecialiteExisteDejaException {
	EcolePolytech ecole=this.getEcole(nomEcole) ;
		 
	Specialite spec = (Specialite) em.find(Specialite.class, nomSpecialite) ;
	if (spec!=null) throw new SpecialiteExisteDejaException() ;
	 if (type==TypeSpec.INITIALE)
        spec=new SpecialiteInitiale() ;
	   else if (type==TypeSpec.APPRENTISSAGE)
		   spec=new SpecialiteApprentissage() ; 
	   else if (type==TypeSpec.FC)
		   spec=new SpecialiteFC() ; 
      spec.setNom(nomSpecialite); 
      spec.setAcronyme(acronyme);
      spec.setEcole(ecole);
      this.em.persist(spec);
    
  }

  
}
  