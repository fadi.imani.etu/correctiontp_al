package ejb.entites;

import java.util.Set;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name="ecole")
public class EcolePolytech implements java.io.Serializable {
  /**
	 * 
	 */
  private static final long serialVersionUID = 1L;
  @Id
  @Column(length=80) private String nom ;
  @Column(name="url", length=100) private String siteWeb ;
  @Column(name="longit") private double longitude ;
  @Column(name="latit") private double latitude ;
  
  @OneToMany(mappedBy="ecole",fetch=FetchType.EAGER) private Set<Specialite> specialites ; 
  
  public EcolePolytech() {}

  
  public String getNom() {
	return nom;
  }

  public void setNom(String nom) {
	this.nom = nom; 
  }
  
  public String getSiteWeb() {
	return siteWeb;
  }

  public void setSiteWeb(String siteWeb) {
	this.siteWeb = siteWeb;
  }
  
  public double getLongitude() {
	return longitude;
  }

  public void setLongitude(double longitude) {
	this.longitude = longitude;
  }
  
  public double getLatitude() {
	return latitude;
  }

  public void setLatitude(double latitude) {
	this.latitude = latitude;
  }


public Set<Specialite> getSpecialites() {
	return specialites;
}


public void setSpecialites(Set<Specialite> specialites) {
	this.specialites = specialites;
}
}
