package ejb.entites;

import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance ;
import jakarta.persistence.InheritanceType ;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;

@Entity
@Inheritance (strategy=InheritanceType.TABLE_PER_CLASS)
public abstract class Specialite implements java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id private String nom ;
	private String acronyme ;
	@ManyToOne() private EcolePolytech ecole ;
	
	public Specialite() {
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
		
	public String getAcronyme() {
		return acronyme;
	}
	public void setAcronyme(String acronyme) {
		this.acronyme = acronyme;
	}
		 
	public EcolePolytech getEcole() {
		return ecole;
	}
	public void setEcole(EcolePolytech ecole) {
		this.ecole = ecole;
	}
}
