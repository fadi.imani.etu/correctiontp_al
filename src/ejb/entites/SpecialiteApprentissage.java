package ejb.entites;



@jakarta.persistence.Entity
public class SpecialiteApprentissage extends Specialite {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public SpecialiteApprentissage() {
		
	}
	public int getTailleLimite() {
		return tailleLimite;
	}
	public void setTailleLimite(int tailleLimite) {
		this.tailleLimite = tailleLimite;
	}
	private int tailleLimite ;
}
