<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Le réseau Polytech</title>
<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css" />
<script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script>


<script type="text/javascript">
  function init_markers() {
	  var plotll, plotmark, url ;
	  var map = new L.Map('map');
	  
	  // Utilisation de OpenStreetmap pour les cartes
	  var osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
	  var osmAttrib='Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
	  var osm = new L.TileLayer(osmUrl, {minZoom: 3, maxZoom: 15, attribution: osmAttrib});
	  map.addLayer(osm);
	  
	  <c:forEach items="${requestScope.ecoles}" var="ecole">
	  
	  
      plotll = new L.LatLng(${ecole.latitude},${ecole.longitude}, true);
      plotmark = new L.Marker(plotll);
      map.addLayer(plotmark);
          
    	codeHTML='<div>' ;
      codeHTML+='<a href="${ecole.siteWeb}">${ecole.nom}</a><br />' ;
      // décommenter cette ligne pour fonctionner avec la météo
      // codeHTML+=httpGet("http://localhost:8080/meteoApp/AffichageMeteo?lang=fr&nom=${ecole.nom}&lat=${ecole.latitude}&lon=${ecole.longitude}");
      codeHTML+='</div>'
  		plotmark.bindPopup(codeHTML);
    </c:forEach> 
      map.setView(plotll,6);
   }
   
  function httpGet(theUrl) {
      var xmlHttp = null;
      xmlHttp = new XMLHttpRequest();
      xmlHttp.open( "GET", theUrl, false );
      xmlHttp.send( null );
      return xmlHttp.responseText;
    }
 </script>

<style type="text/css">
#map {
	height: 600px;
}
</style>

</head>
<body  onLoad="init_markers();">
<header>Le réseau Polytech </header>
<section>
  <div id="map"> </div>
</section>   

</body>
</html>




