<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ajout spécialité</title>
</head>
<body>
<h1>Ajout de spécialité</h1>
<b>${requestScope.message}</b>

<c:if test = "${not empty requestScope.ecole}">
 <h3>Les spécialités de ${ecole.nom}</h3>
 <ul>
   <c:forEach var="spec" items="${requestScope.ecole.specialites}">
        <li>${spec.nom} (${spec.acronyme})</li>
   </c:forEach>
 </ul>
</c:if>
</body>
</html>