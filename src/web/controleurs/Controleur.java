package web.controleurs;

import java.io.IOException;
import java.util.Collection;

import ejb.sessions.* ;
import ejb.entites.* ;
import ejb.sessions.ServiceEcolesPolytech.TypeSpec ;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;


@WebServlet(value={"formAjoutSpecialite","ajoutSpecialite","carte"})
public class Controleur extends HttpServlet {
  private static final long serialVersionUID = 1L;
  
  @jakarta.ejb.EJB private ServiceEcolesPolytechLocal service ;
  
  public Controleur() {}
  
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {
    String url = request.getRequestURL().toString();
	String maVue ="/carte.jsp"; // vue par défaut
	if (url.endsWith("/carte")) {
		maVue ="/carte.jsp"; 
		Collection<EcolePolytech> ecoles=service.getEcolesPolytech();
		request.setAttribute("ecoles", ecoles);		
	} else if (url.endsWith("formAjoutSpecialite")){
		maVue="/formAjoutSpecialite.html" ;
	} else if (url.endsWith("ajoutSpecialite")){
	   String nomEcole=request.getParameter("nomEcole") ;
	   String nomSpecialite=request.getParameter("nomSpecialite");
	   String acronyme=request.getParameter("acronymeSpecialite");
	   String typeSpecialite=request.getParameter("typeSpecialite");
	   TypeSpec type ;
	   if (typeSpecialite.equals("initiale")) type=TypeSpec.INITIALE ;
	   else if (typeSpecialite.equals("apprentissage")) type=TypeSpec.APPRENTISSAGE ;
	   else type=TypeSpec.FC ;
	   maVue="/ajoutSpecialite.jsp" ;
	   String message ;
	   try {
		service.addSpecialite(nomEcole, nomSpecialite, acronyme, type);
		message="création de la spécialité "+nomSpecialite+" dans école "+nomEcole ;
	} catch (EcoleInconnueException e) {
		message="école inconnue : "+nomEcole ;
	} catch (SpecialiteExisteDejaException e) {
		message="spécialité "+nomSpecialite+" existe déjà" ;
	}
	request.setAttribute("message", message);
	EcolePolytech ecole=null ;  
	try {
	  ecole=service.getEcole(nomEcole);
	} catch (EcoleInconnueException e) {}
	request.setAttribute("ecole", ecole);   
	}
	RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(maVue);
    dispatcher.forward(request,response);
  }
}
